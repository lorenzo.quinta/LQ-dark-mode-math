# LQ-dark-mode
This is a dark-themed stylesheet for Wolfram Mathematica that I personally find more visually pleasing than the default *ReverseColor* stylesheet. This was mainly tested in Mathematica 13.0.
Examples of visuals of this stylesheet are under the `Images` folder.

## How to load on a single notebook
The basic way to load the stylesheet on a single notebook is to open that notebook, select `Format->Stylesheet->Other`, and select the path to the file `LQ_Dark_Mode.nb`.

## How to make the stylesheet available among the default ones
To make the stylesheet available among the pre-existing ones in the dropdown menu `Format->Stylesheets`, simply copy the file `LQ_Dark_Mode.nb` to the appropriate folder for default stylesheets. It can be recognised as being the folder where the default stylesheet `ReverseColor.nb` is saved.

In **Windows** the target folder is:

```C:\Users\yourusername\AppData\Roaming\Mathematica\SystemFiles\FrontEnd\StyleSheets```

For **Linux/MacOS** systems the target folder should look like:

```/usr/local/Wolfram/Mathematica/xx.x/SystemFiles/FrontEnd/StyleSheets```

and requires administrator privileges to write to.

## How set this stylesheet as default for new notebooks
Regardless of whether the stylesheet appears under the dropdown list under `Format->Stylesheets`, it can be set as default one for new notebooks. 

To do so, open `Format->Option Inspector`, change `Show Option Values` to *Global Preferences* and lookup the Option `DefaultStyleDefinitions`. Press on the three dots next to that option and insert the path to the stylesheet `LQ_Dark_Mode.nb`.

