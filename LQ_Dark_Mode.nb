(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     24743,        615]
NotebookOptionsPosition[     17159,        520]
NotebookOutlinePosition[     17597,        537]
CellTagsIndexPosition[     17554,        534]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["\<\
stylesheet created by Lorenzo Quintavalle
https://gitlab.com/lorenzo.quinta/LQ-dark-mode-math\
\>", "Text",
 CellChangeTimes->{{3.894331288713764*^9, 
  3.8943312960564127`*^9}},ExpressionUUID->"e51cb3b5-642c-42e7-ab31-\
83d774bded15"],

Cell[StyleData[StyleDefinitions -> "Default.nb"],
 Background->RGBColor[
  0.972396, 0.987778, 
   1],ExpressionUUID->"360fbdcf-c78a-4628-9826-7c188edbfa14"],

Cell[CellGroupData[{

Cell["Notebook Options Settings", "Section",
 CellChangeTimes->{{3.601921410547186*^9, 3.601921410758452*^9}, {
  3.603061388111863*^9, 3.60306138967712*^9}, {3.60306172718675*^9, 
  3.60306172728065*^9}, {3.603061922086404*^9, 3.603061922267686*^9}, {
  3.603062579432806*^9, 3.6030625797175283`*^9}, {3.6035645489732723`*^9, 
  3.603564549378281*^9}, {3.603564623539118*^9, 3.6035646470665607`*^9}, {
  3.6035654507014523`*^9, 3.6035654511384974`*^9}, {3.6077703945903893`*^9, 
  3.6077703947453947`*^9}, {3.696770164204349*^9, 
  3.696770164519588*^9}},ExpressionUUID->"09f99da6-01d5-4eb8-ae1b-\
b0a82000e889"],

Cell["\<\
The options defined for the style below will be used at the Notebook level.\
\>", "Text",ExpressionUUID->"4fdc0f12-70bb-4cf8-916d-b711e9b8ba7c"],

Cell[StyleData["Notebook"],
 CellBracketOptions->{"Color"->RGBColor[0.3, 0.3, 0.3],
 "HoverColor"->RGBColor[
  0.14901960784313725`, 0.5450980392156862, 0.8235294117647058]},
 InitializationCell->False,
 CellChangeTimes->{{3.8203220314950294`*^9, 3.8203220328234777`*^9}, {
   3.8203224874098167`*^9, 3.820322488695381*^9}, {3.8203227258318987`*^9, 
   3.820322726367435*^9}, {3.8203229019752197`*^9, 3.8203229024958267`*^9}, {
   3.8203229594124436`*^9, 3.8203229618279877`*^9}, 3.820479468648271*^9},
 AutoStyleOptions->{"CommentStyle"->{
   RGBColor[0.709804, 0.717647, 0.45098], FontFamily -> "Source Serif Pro", 
   FontSlant -> "Italic", ShowAutoStyles -> False, ShowSyntaxStyles -> False, 
   AutoNumberFormatting -> False},
 "ExcessArgumentStyle"->{
  FontColor -> 
   RGBColor[0.8627450980392157, 0.19607843137254902`, 0.1843137254901961]},
 "FunctionLocalVariableStyle"->{FontColor -> RGBColor[0.717647, 1, 0.717647]},
 "GraphicsCompatibilityProblemStyle"->{
  FontColor -> 
   RGBColor[0.8627450980392157, 0.19607843137254902`, 0.1843137254901961]},
 "LocalScopeConflictStyle"->{
  FontColor -> 
   RGBColor[0.8274509803921568, 0.21176470588235294`, 0.5098039215686274]},
 "LocalVariableStyle"->{FontColor -> RGBColor[0.717647, 1, 0.717647]},
 "MissingArgumentStyle"->{
  FontColor -> 
   RGBColor[0.8627450980392157, 0.19607843137254902`, 0.1843137254901961]},
 "MisspelledWordStyle"->{
  FontColor -> 
   RGBColor[0.8274509803921568, 0.21176470588235294`, 0.5098039215686274]},
 "NoKernelPresentStyle"->{
  FontColor -> 
   RGBColor[0.16470588235294117`, 0.6313725490196078, 0.596078431372549]},
 "PatternVariableStyle"->{
  FontColor -> RGBColor[0.486275, 0.72549, 0.619608], FontSlant -> "Italic"},
 "StringStyle"->{
  FontColor -> RGBColor[0.552941, 0.443137, 0.72549], ShowAutoStyles -> False,
    ShowSyntaxStyles -> False, AutoNumberFormatting -> False},
 "UndefinedSymbolStyle"->RGBColor[0.54902, 0.690196, 0.862745],
 "UnknownOptionStyle"->{
  FontColor -> 
   RGBColor[0.8627450980392157, 0.19607843137254902`, 0.1843137254901961]},
 "UnwantedAssignmentStyle"->{
  FontColor -> 
   RGBColor[0.8627450980392157, 0.19607843137254902`, 0.1843137254901961]}},
 CodeAssistOptions->{
 "HeadHighlightStyle"->{
  Background -> RGBColor[0.117647, 0.466667, 0.266667], FontColor -> 
   RGBColor[0.9333333333333333, 0.9098039215686274, 0.8352941176470589]},
 "MatchHighlightStyle"->{
  Background -> RGBColor[0.7098039215686275, 0.5372549019607843, 0.], 
   FontColor -> 
   RGBColor[0.9333333333333333, 0.9098039215686274, 0.8352941176470589]}},
 StyleMenuListing->None,
 FontColor->RGBColor[0.8, 0.8, 0.8],
 Background->RGBColor[
  0.14901960784313725`, 0.14901960784313725`, 
   0.14901960784313725`],ExpressionUUID->"acc05ee5-8985-46d4-9bea-\
ac0ebca40ada"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Styles for Title and Section Cells", "Section",ExpressionUUID->"0ec8e6b8-78ca-49be-bb93-0dd7794e227e"],

Cell[StyleData["Title"],
 CellFrame->{{0, 0}, {0, 1}},
 CellDingbat->None,
 CellMargins->{{25, Inherited}, {1, 3}},
 CellFrameColor->GrayLevel[0.4],
 FontFamily->"Source Serif Pro",
 FontSize->45,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->-0.5,
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.7450980392156863, 0.12941176470588237`, 0.11372549019607843`],
 Background->None,ExpressionUUID->"ab24814d-031f-459e-8593-4306a9ce7357"],

Cell[StyleData["Chapter"],
 CellFrame->{{0, 0}, {0, 0}},
 CellMargins->{{25, Inherited}, {1, 2}},
 CellFrameColor->GrayLevel[0.7],
 FontColor->RGBColor[
  0.8, 0.8, 0.8],ExpressionUUID->"722a3c5c-85e6-4a28-993f-ddd16761ecd3"],

Cell[StyleData["Subchapter"],
 CellMargins->{{63, Inherited}, {1, 2}},
 FontColor->RGBColor[
  0.65, 0.65, 0.65],ExpressionUUID->"3ae32c22-dc66-4c5f-8a6c-98979adf8db0"],

Cell[StyleData["Subtitle"],
 CellMargins->{{40, Inherited}, {1, 2}},
 FontFamily->"Source Serif Pro",
 FontSize->24,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.796078, 0.294118, 
   0.0862745],ExpressionUUID->"5950002b-40df-4bae-84f3-68ad548c8330"],

Cell[StyleData["Subsubtitle"],
 CellMargins->{{40, Inherited}, {1, 2}},
 FontFamily->"Source Serif Pro",
 FontSize->16,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.852811, 0.696178, 
   0.0932479],ExpressionUUID->"909811f2-19a4-4bd2-beb7-3235f7ae6d0d"],

Cell[StyleData["Section"],
 CellFrame->{{0, 0}, {0, 0}},
 CellMargins->{{35, Inherited}, {1, 2}},
 FontFamily->"Source Serif Pro",
 FontSize->28,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.8235294117647058, 0.27450980392156865`, 
   0.11764705882352941`],ExpressionUUID->"e57dbe61-bbd1-461e-bbd2-\
e7a34bdd4d97"],

Cell[StyleData["Subsection"],
 CellMargins->{{45, 3.}, {3, 8}},
 FontFamily->"Source Serif Pro",
 FontSize->20,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.8235294117647058, 0.4, 
   0.11764705882352941`],ExpressionUUID->"876313c2-12b4-4eae-a388-\
80fac0b1abbd"],

Cell[StyleData["Subsubsection"],
 CellMargins->{{55, Inherited}, {3, 8}},
 FontFamily->"Source Serif Pro",
 FontSize->19,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.8196078431372549, 0.5411764705882353, 
   0.12156862745098039`],ExpressionUUID->"81769868-1275-4944-bdc6-\
49ca9eadc88d"],

Cell[StyleData["Subsubsubsection"],
 CellMargins->{{55, Inherited}, {3, 8}},
 FontFamily->"Source Serif Pro",
 FontSize->15,
 FontWeight->"Bold",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.954147, 0.694224, 
   0.124651],ExpressionUUID->"a82ba479-0d2f-4c48-b3d2-d64ba6e4098f"],

Cell[StyleData["Subsubsubsubsection"],
 CellMargins->{{55, Inherited}, {3, 8}},
 FontFamily->"Source Serif Pro",
 FontSize->14,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.913237, 0.365469, 
   0.0854353],ExpressionUUID->"7e762d47-2cb1-47e7-b596-a41c84d78ec4"],

Cell[StyleData["Text"],
 CellMargins->{{55, 10}, {1, 5}},
 FontFamily->"Calibri",
 FontSize->14,
 FontWeight->"Normal",
 FontSlant->"Plain",
 FontTracking->"Plain",
 FontVariations->{"StrikeThrough"->False,
 "Underline"->False},
 FontColor->RGBColor[
  0.709804, 0.717647, 
   0.45098],ExpressionUUID->"641a3cde-13e7-4d9e-b727-9b92f04e2977"],

Cell[StyleData["ItemParagraph"],
 FontColor->GrayLevel[
  0.6],ExpressionUUID->"60ea7103-d91c-4b78-886b-6bdfc16b5577"],

Cell[StyleData["SubitemParagraph"],
 FontColor->GrayLevel[
  0.6],ExpressionUUID->"fe42aaf6-7f86-412a-ac32-32b538a8b564"],

Cell[StyleData["SubsubitemParagraph"],
 FontColor->GrayLevel[
  0.6],ExpressionUUID->"8e877ca4-6c04-4643-9e60-d422b130e10e"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Styles for Input and Output Cells", "Section",ExpressionUUID->"e3a9aad9-6c5d-4212-a72a-d7d418472829"],

Cell[StyleData["Input"],
 CellMargins->{{60, 10}, {0, 5}},
 FontColor->RGBColor[
  0.7529411764705882, 0.7529411764705882, 0.7529411764705882],
 Background->RGBColor[
  0.0784313725490196, 0.0784313725490196, 
   0.0784313725490196],ExpressionUUID->"46d0e7c3-5133-47ff-8ea6-fd6e6117812e"],

Cell[StyleData["Output"],
 CellMargins->{{60, 10}, {3, 2}},
 FontColor->RGBColor[
  0.7529411764705882, 0.7529411764705882, 0.7529411764705882],
 Background->RGBColor[
  0.23529411764705882`, 0.23529411764705882`, 0.23529411764705882`],
 "ContentAreaBackground" -> 
 RGBColor[0.94, 0.88, 
   0.94],ExpressionUUID->"468907a4-a827-4e09-9274-f1f0656abf36"],

Cell[StyleData["EchoBefore"],
 FontColor->GrayLevel[
  0.6],ExpressionUUID->"9293149f-b72e-4296-819d-3e48155af9fc"],

Cell[StyleData["InitializationCell"],
 CellDingbat->None,
 FontColor->GrayLevel[0.9],
 Background->RGBColor[
  0.1568627450980392, 0.26666666666666666`, 
   0.26666666666666666`],ExpressionUUID->"48448450-480e-42c5-b696-\
fa9cf0b8402f"],

Cell[StyleData["Message"],
 FontColor->RGBColor[
  0.704600595101854, 0.24277103837644007`, 
   0.22278171969176777`],ExpressionUUID->"8c6803ea-d0b9-4213-b12b-\
10e97ab07ee0"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Styles for Formulas and Programming", "Section",ExpressionUUID->"5626d910-2ed2-464b-8c92-9f9a576df206"],

Cell[StyleData["DisplayFormula"],
 FontColor->GrayLevel[
  0.85],ExpressionUUID->"a3e9283b-a05f-49a9-ae1b-ce8bda922339"],

Cell[StyleData["DisplayFormulaNumbered"],
 FontColor->GrayLevel[
  0.85],ExpressionUUID->"895a7b07-910e-4492-8255-d28c2ba622c2"],

Cell[StyleData["Program"],
 CellFrameColor->GrayLevel[
  0.4],ExpressionUUID->"d043f363-7c84-4b08-9bb8-c9b4398ff2a6"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "Styles for ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 " System\[Hyphen]specific Elements"
}], "Section",
 CellChangeTimes->{{3.601920784160323*^9, 3.60192078489342*^9}, 
   3.60356525124664*^9, {3.6037117681643677`*^9, 
   3.6037117683195887`*^9}},ExpressionUUID->"864f2fae-a72c-4d7f-b3ad-\
65383b16252c"],

Cell[CellGroupData[{

Cell["Default Box Styles", "Subsection",ExpressionUUID->"f1b18d0e-b82e-4f51-becd-3679e868dce7"],

Cell[StyleData["Manipulate"],
 PanelBoxOptions->{Background->GrayLevel[0.35]},
 "ContentAreaBackground" -> 
 GrayLevel[0.25],ExpressionUUID->"e436414f-0e2f-4d6a-88f2-077093e4ef58"],

Cell[StyleData["Setter"],
 FontColor->GrayLevel[
  0.1],ExpressionUUID->"20c41d82-6b32-4da7-952f-d82c7a2da198"],

Cell[StyleData["InputField"],
 FontColor->GrayLevel[
  0.1],ExpressionUUID->"c368e51c-060b-4d68-988f-ce4119ff66ba"]
}, Open  ]],

Cell[CellGroupData[{

Cell["TemplateBox Styles", "Subsection",ExpressionUUID->"209d936d-111c-45ae-a655-d594afc04f6a"],

Cell[CellGroupData[{

Cell["Control system functions", "Subsubsection",ExpressionUUID->"03a96a27-a45f-49d1-b9b8-b985f3b4f95a"],

Cell[StyleData["SystemsModelGrid00"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {{None, 
      GrayLevel[
      0.4]}}}},ExpressionUUID->"d78bf32f-62a4-44a7-8598-f610f019a5b2"],

Cell[StyleData["SystemsModelGrid01"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {{None, 
      GrayLevel[0.4]}}},
 GridBoxItemStyle->{"Rows" -> {{Automatic}}, "Columns" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {
     Automatic}}}},ExpressionUUID->"a462f63e-5aac-42ec-a8a3-5f42b74928f4"],

Cell[StyleData["SystemsModelGrid02"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {{None, 
      GrayLevel[0.4]}}},
 GridBoxItemStyle->{"Rows" -> {{Automatic}}, "Columns" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], 
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {
     Automatic}}}},ExpressionUUID->"33f4a995-773d-4d5c-989c-3198a2ea1172"],

Cell[StyleData["SystemsModelGrid10"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {None, {None, 
      GrayLevel[0.4]}}},
 GridBoxItemStyle->{"Rows" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {Automatic}}, 
   "Columns" -> {{
     Automatic}}}},ExpressionUUID->"f35ef974-d7af-42a2-9986-19db78c78d68"],

Cell[StyleData["SystemsModelGrid11"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {None, {None, 
      GrayLevel[0.4]}}},
 GridBoxItemStyle->{"Rows" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {Automatic}}, "Columns" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {
     Automatic}}}},ExpressionUUID->"0a456d27-b94d-4911-aaf0-e13542832b60"],

Cell[StyleData["SystemsModelGrid12"],
 GridBoxOptions->{AllowScriptLevelChange->False,
 GridBoxBackground->{"Rows" -> {None, {None, 
      GrayLevel[0.4]}}},
 GridBoxItemStyle->{"Rows" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {Automatic}}, "Columns" -> {
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], 
     Directive["InlineText", Smaller, 
      GrayLevel[0.7]], {
     Automatic}}}},ExpressionUUID->"14a77ec7-f4d3-4ec1-a954-082457db55d8"],

Cell[StyleData["SystemsModelSuperscript"],
 FrontFaceColor->GrayLevel[0.4],
 DrawEdges->False,
 FontColor->GrayLevel[1],
 RectangleBoxOptions->{
 RoundingRadius->Offset[
  2.5]},ExpressionUUID->"4a648fa7-e489-4625-8d88-b4f263c235e6"],

Cell[StyleData["SystemsModelMinimalElement"],
 ShowSyntaxStyles->False,
 FontColor->GrayLevel[
  0.7],ExpressionUUID->"8bf52e45-fad4-4ef7-90ec-202b7d0d03e4"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["FormatType Styles", "Subsection",ExpressionUUID->"3c960771-6fac-4dc8-ac24-e4e2955b72ef"],

Cell[StyleData["StandardForm"],ExpressionUUID->"abefa211-d7bf-4b4e-805d-8c1a0b7c5a82"],

Cell[StyleData["CellExpression"],
 FontColor->GrayLevel[0],
 Background->GrayLevel[
  0.75],ExpressionUUID->"d91761e3-1fd9-4f62-bf3a-a80e491789fb"],

Cell[StyleData["CellLabel"],
 FontSize->9,
 FontColor->RGBColor[
  0.603861, 0.615915, 
   0.607843],ExpressionUUID->"8866b161-d800-4955-9596-22d4f431ebfc"],

Cell["\<\
The style defined below is mixed in to any cell that is in an inline cell \
within another.\
\>", "Text",ExpressionUUID->"8dc28ede-69af-4ade-a210-2d282dfaa27e"],

Cell[StyleData["InlineCellEditing"],
 StyleMenuListing->None,
 Background->RGBColor[
  0.178, 0.258, 0.396],ExpressionUUID->"7ef4ed11-358c-41ac-92ac-f1f145304833"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Graphics Styles", "Subsection",
 CellChangeTimes->{{3.603565272271762*^9, 
  3.603565284491351*^9}},ExpressionUUID->"5e070be8-2614-4cdc-aa6c-\
ab0efec5435a"],

Cell[StyleData["GraphicsAxes"],
 LineColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 0.6313725490196078],
 FontFamily->"Avenir",
 FontSize->9,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 
   0.6313725490196078],ExpressionUUID->"75892cbd-22e3-432e-b81f-4b5928ec02df"],

Cell[StyleData["GraphicsFrame"],
 LineColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 0.6313725490196078],
 FontFamily->"Avenir",
 FontSize->9,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 
   0.6313725490196078],ExpressionUUID->"60eaa1dd-9307-4d04-b7df-f8dc7b2914f6"],

Cell[StyleData["Graphics3DAxes"],
 LineColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 0.6313725490196078],
 FontFamily->"Arial",
 FontSize->9,
 FontWeight->"Plain",
 FontSlant->"Plain",
 FontColor->RGBColor[
  0.5764705882352941, 0.6313725490196078, 
   0.6313725490196078],ExpressionUUID->"e512d949-e89f-4baf-a096-0f942893d30e"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Additions", "Section",
 CellChangeTimes->{{3.820302313326337*^9, 
  3.8203023151524496`*^9}},ExpressionUUID->"4dc4d1c0-beec-4390-872e-\
5608c15b6e67"],

Cell[StyleData["OutputSizeLimit"],
 Background->RGBColor[
  0.23529411764705882`, 0.23529411764705882`, 
   0.23529411764705882`],ExpressionUUID->"f5f4d0ea-6611-4ea6-91f1-\
e93747b60490"]
}, Open  ]]
},
WindowSize->{579., 570.6},
WindowMargins->{{
  Automatic, 272.4000000000001}, {-9.600000000000023, Automatic}},
FrontEndVersion->"13.0 for Microsoft Windows (64-bit) (February 4, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"8611d972-4f9e-4524-8053-e5509a9f4ad6"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 245, 6, 58, "Text",ExpressionUUID->"e51cb3b5-642c-42e7-ab31-83d774bded15"],
Cell[806, 28, 157, 3, 41, 49, 0, "StyleData", "StyleDefinitions", "",ExpressionUUID->"360fbdcf-c78a-4628-9826-7c188edbfa14"],
Cell[CellGroupData[{
Cell[988, 35, 613, 9, 67, "Section",ExpressionUUID->"09f99da6-01d5-4eb8-ae1b-b0a82000e889"],
Cell[1604, 46, 154, 2, 35, "Text",ExpressionUUID->"4fdc0f12-70bb-4cf8-916d-b711e9b8ba7c"],
Cell[1761, 50, 2772, 58, 52, 27, 0, "StyleData", "Notebook", "All",ExpressionUUID->"acc05ee5-8985-46d4-9bea-ac0ebca40ada",
 StyleMenuListing->None]
}, Open  ]],
Cell[CellGroupData[{
Cell[4570, 113, 108, 0, 67, "Section",ExpressionUUID->"0ec8e6b8-78ca-49be-bb93-0dd7794e227e"],
Cell[4681, 115, 487, 14, 84, 24, 0, "StyleData", "Title", "All",ExpressionUUID->"ab24814d-031f-459e-8593-4306a9ce7357"],
Cell[5171, 131, 225, 5, 60, 26, 0, "StyleData", "Chapter", "All",ExpressionUUID->"722a3c5c-85e6-4a28-993f-ddd16761ecd3"],
Cell[5399, 138, 168, 3, 53, 29, 0, "StyleData", "Subchapter", "All",ExpressionUUID->"3ae32c22-dc66-4c5f-8a6c-98979adf8db0"],
Cell[5570, 143, 362, 11, 48, 27, 0, "StyleData", "Subtitle", "All",ExpressionUUID->"5950002b-40df-4bae-84f3-68ad548c8330"],
Cell[5935, 156, 365, 11, 38, 30, 0, "StyleData", "Subsubtitle", "All",ExpressionUUID->"909811f2-19a4-4bd2-beb7-3235f7ae6d0d"],
Cell[6303, 169, 426, 13, 53, 26, 0, "StyleData", "Section", "All",ExpressionUUID->"e57dbe61-bbd1-461e-bbd2-e7a34bdd4d97"],
Cell[6732, 184, 375, 12, 47, 29, 0, "StyleData", "Subsection", "All",ExpressionUUID->"876313c2-12b4-4eae-a388-80fac0b1abbd"],
Cell[7110, 198, 400, 12, 45, 32, 0, "StyleData", "Subsubsection", "All",ExpressionUUID->"81769868-1275-4944-bdc6-49ca9eadc88d"],
Cell[7513, 212, 368, 11, 40, 35, 0, "StyleData", "Subsubsubsection", "All",ExpressionUUID->"a82ba479-0d2f-4c48-b3d2-d64ba6e4098f"],
Cell[7884, 225, 373, 11, 39, 38, 0, "StyleData", "Subsubsubsubsection", "All",ExpressionUUID->"7e762d47-2cb1-47e7-b596-a41c84d78ec4"],
Cell[8260, 238, 341, 11, 37, 23, 0, "StyleData", "Text", "All",ExpressionUUID->"641a3cde-13e7-4d9e-b727-9b92f04e2977"],
Cell[8604, 251, 118, 2, 38, 32, 0, "StyleData", "ItemParagraph", "All",ExpressionUUID->"60ea7103-d91c-4b78-886b-6bdfc16b5577"],
Cell[8725, 255, 121, 2, 37, 35, 0, "StyleData", "SubitemParagraph", "All",ExpressionUUID->"fe42aaf6-7f86-412a-ac32-32b538a8b564"],
Cell[8849, 259, 124, 2, 36, 38, 0, "StyleData", "SubsubitemParagraph", "All",ExpressionUUID->"8e877ca4-6c04-4643-9e60-d422b130e10e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9010, 266, 107, 0, 67, "Section",ExpressionUUID->"e3a9aad9-6c5d-4212-a72a-d7d418472829"],
Cell[9120, 268, 288, 6, 50, 24, 0, "StyleData", "Input", "All",ExpressionUUID->"46d0e7c3-5133-47ff-8ea6-fd6e6117812e"],
Cell[9411, 276, 353, 8, 50, 25, 0, "StyleData", "Output", "All",ExpressionUUID->"468907a4-a827-4e09-9274-f1f0656abf36"],
Cell[9767, 286, 115, 2, 36, 29, 0, "StyleData", "EchoBefore", "All",ExpressionUUID->"9293149f-b72e-4296-819d-3e48155af9fc"],
Cell[9885, 290, 236, 6, 52, 37, 0, "StyleData", "InitializationCell", "All",ExpressionUUID->"48448450-480e-42c5-b696-fa9cf0b8402f"],
Cell[10124, 298, 175, 4, 37, 26, 0, "StyleData", "Message", "All",ExpressionUUID->"8c6803ea-d0b9-4213-b12b-10e97ab07ee0"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10336, 307, 109, 0, 67, "Section",ExpressionUUID->"5626d910-2ed2-464b-8c92-9f9a576df206"],
Cell[10448, 309, 120, 2, 40, 33, 0, "StyleData", "DisplayFormula", "All",ExpressionUUID->"a3e9283b-a05f-49a9-ae1b-ce8bda922339"],
Cell[10571, 313, 128, 2, 40, 41, 0, "StyleData", "DisplayFormulaNumbered", "All",ExpressionUUID->"895a7b07-910e-4492-8255-d28c2ba622c2"],
Cell[10702, 317, 117, 2, 62, 26, 0, "StyleData", "Program", "All",ExpressionUUID->"d043f363-7c84-4b08-9bb8-c9b4398ff2a6"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10856, 324, 333, 9, 105, "Section",ExpressionUUID->"864f2fae-a72c-4d7f-b3ad-65383b16252c"],
Cell[CellGroupData[{
Cell[11214, 337, 95, 0, 54, "Subsection",ExpressionUUID->"f1b18d0e-b82e-4f51-becd-3679e868dce7"],
Cell[11312, 339, 180, 3, 36, 29, 0, "StyleData", "Manipulate", "All",ExpressionUUID->"e436414f-0e2f-4d6a-88f2-077093e4ef58"],
Cell[11495, 344, 111, 2, 36, 25, 0, "StyleData", "Setter", "All",ExpressionUUID->"20c41d82-6b32-4da7-952f-d82c7a2da198"],
Cell[11609, 348, 115, 2, 36, 29, 0, "StyleData", "InputField", "All",ExpressionUUID->"c368e51c-060b-4d68-988f-ce4119ff66ba"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11761, 355, 95, 0, 54, "Subsection",ExpressionUUID->"209d936d-111c-45ae-a655-d594afc04f6a"],
Cell[CellGroupData[{
Cell[11881, 359, 104, 0, 45, "Subsubsection",ExpressionUUID->"03a96a27-a45f-49d1-b9b8-b985f3b4f95a"],
Cell[11988, 361, 214, 4, 36, 37, 0, "StyleData", "SystemsModelGrid00", "All",ExpressionUUID->"d78bf32f-62a4-44a7-8598-f610f019a5b2"],
Cell[12205, 367, 350, 7, 36, 37, 0, "StyleData", "SystemsModelGrid01", "All",ExpressionUUID->"a462f63e-5aac-42ec-a8a3-5f42b74928f4"],
Cell[12558, 376, 413, 9, 36, 37, 0, "StyleData", "SystemsModelGrid02", "All",ExpressionUUID->"33f4a995-773d-4d5c-989c-3198a2ea1172"],
Cell[12974, 387, 360, 8, 36, 37, 0, "StyleData", "SystemsModelGrid10", "All",ExpressionUUID->"f35ef974-d7af-42a2-9986-19db78c78d68"],
Cell[13337, 397, 419, 9, 36, 37, 0, "StyleData", "SystemsModelGrid11", "All",ExpressionUUID->"0a456d27-b94d-4911-aaf0-e13542832b60"],
Cell[13759, 408, 482, 11, 36, 37, 0, "StyleData", "SystemsModelGrid12", "All",ExpressionUUID->"14a77ec7-f4d3-4ec1-a954-082457db55d8"],
Cell[14244, 421, 233, 6, 36, 42, 0, "StyleData", "SystemsModelSuperscript", "All",ExpressionUUID->"4a648fa7-e489-4625-8d88-b4f263c235e6"],
Cell[14480, 429, 157, 3, 36, 45, 0, "StyleData", "SystemsModelMinimalElement", "All",ExpressionUUID->"8bf52e45-fad4-4ef7-90ec-202b7d0d03e4"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14686, 438, 94, 0, 54, "Subsection",ExpressionUUID->"3c960771-6fac-4dc8-ac24-e4e2955b72ef"],
Cell[14783, 440, 86, 0, 37, 31, 0, "StyleData", "StandardForm", "All",ExpressionUUID->"abefa211-d7bf-4b4e-805d-8c1a0b7c5a82"],
Cell[14872, 442, 147, 3, 53, 33, 0, "StyleData", "CellExpression", "All",ExpressionUUID->"d91761e3-1fd9-4f62-bf3a-a80e491789fb"],
Cell[15022, 447, 156, 4, 33, 28, 0, "StyleData", "CellLabel", "All",ExpressionUUID->"8866b161-d800-4955-9596-22d4f431ebfc"],
Cell[15181, 453, 170, 3, 58, "Text",ExpressionUUID->"8dc28ede-69af-4ade-a210-2d282dfaa27e"],
Cell[15354, 458, 163, 3, 52, 36, 0, "StyleData", "InlineCellEditing", "All",ExpressionUUID->"7ef4ed11-358c-41ac-92ac-f1f145304833",
 StyleMenuListing->None]
}, Open  ]],
Cell[CellGroupData[{
Cell[15554, 466, 163, 3, 54, "Subsection",ExpressionUUID->"5e070be8-2614-4cdc-aa6c-ab0efec5435a"],
Cell[15720, 471, 340, 9, 36, 31, 0, "StyleData", "GraphicsAxes", "All",ExpressionUUID->"75892cbd-22e3-432e-b81f-4b5928ec02df"],
Cell[16063, 482, 341, 9, 36, 32, 0, "StyleData", "GraphicsFrame", "All",ExpressionUUID->"60eaa1dd-9307-4d04-b7df-f8dc7b2914f6"],
Cell[16407, 493, 341, 9, 33, 33, 0, "StyleData", "Graphics3DAxes", "All",ExpressionUUID->"e512d949-e89f-4baf-a096-0f942893d30e"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[16797, 508, 156, 3, 67, "Section",ExpressionUUID->"4dc4d1c0-beec-4390-872e-5608c15b6e67"],
Cell[16956, 513, 187, 4, 52, 34, 0, "StyleData", "OutputSizeLimit", "All",ExpressionUUID->"f5f4d0ea-6611-4ea6-91f1-e93747b60490"]
}, Open  ]]
}
]
*)

